 #/$$$$$$                        /$$                                    /$$$$$$                               
 #/$$__  $$                      | $$                                   /$$__  $$                              
#| $$  \__/ /$$   /$$  /$$$$$$$ /$$$$$$    /$$$$$$  /$$$$$$/$$$$       | $$  \__/  /$$$$$$   /$$$$$$   /$$$$$$ 
#|  $$$$$$ | $$  | $$ /$$_____/|_  $$_/   /$$__  $$| $$_  $$_  $$      | $$       /$$__  $$ /$$__  $$ /$$__  $$
# \____  $$| $$  | $$|  $$$$$$   | $$    | $$$$$$$$| $$ \ $$ \ $$      | $$      | $$  \ $$| $$  \__/| $$$$$$$$
# /$$  \ $$| $$  | $$ \____  $$  | $$ /$$| $$_____/| $$ | $$ | $$      | $$    $$| $$  | $$| $$      | $$_____/
#|  $$$$$$/|  $$$$$$$ /$$$$$$$/  |  $$$$/|  $$$$$$$| $$ | $$ | $$      |  $$$$$$/|  $$$$$$/| $$      |  $$$$$$$
# \______/  \____  $$|_______/    \___/   \_______/|__/ |__/ |__/       \______/  \______/ |__/       \_______/
#           /$$  | $$                                                                                          
#          |  $$$$$$/                                                                                          
#           \______/                      
#system-core

sudo zypper install flatpak && flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo


#fastfetch
sudo zypper install fastfetch -y 

#nmap
sudo zypper install nmap -y

#xkill 
sudo zypper install xkill -y

#x11vnc
sudo zypper install x11vnc -y

#kupfer launcher
sudo zypper install kupfer -y

#my fav terminal 
sudo zypper install tilix -y 

#distrobox
sudo zypper install distrobox -y


# /$$$$$$                        /$$                                    /$$$$$$        /$$       /$$                              
# /$$__  $$                      | $$                                   /$$__  $$      | $$      | $$                              
#| $$  \__/ /$$   /$$  /$$$$$$$ /$$$$$$    /$$$$$$  /$$$$$$/$$$$       | $$  \ $$  /$$$$$$$  /$$$$$$$  /$$$$$$  /$$$$$$$   /$$$$$$$
#|  $$$$$$ | $$  | $$ /$$_____/|_  $$_/   /$$__  $$| $$_  $$_  $$      | $$$$$$$$ /$$__  $$ /$$__  $$ /$$__  $$| $$__  $$ /$$_____/
# \____  $$| $$  | $$|  $$$$$$   | $$    | $$$$$$$$| $$ \ $$ \ $$      | $$__  $$| $$  | $$| $$  | $$| $$  \ $$| $$  \ $$|  $$$$$$ 
# /$$  \ $$| $$  | $$ \____  $$  | $$ /$$| $$_____/| $$ | $$ | $$      | $$  | $$| $$  | $$| $$  | $$| $$  | $$| $$  | $$ \____  $$
#|  $$$$$$/|  $$$$$$$ /$$$$$$$/  |  $$$$/|  $$$$$$$| $$ | $$ | $$      | $$  | $$|  $$$$$$$|  $$$$$$$|  $$$$$$/| $$  | $$ /$$$$$$$/
# \______/  \____  $$|_______/    \___/   \_______/|__/ |__/ |__/      |__/  |__/ \_______/ \_______/ \______/ |__/  |__/|_______/ 
#           /$$  | $$                                                                                                              
#          |  $$$$$$/                                                                                                              
#          \______/                                
#
#system-addons like extensions and secondary system requirements. 


#tailscale vpn
curl -fsSL https://tailscale.com/install.sh | sh
sudo zypper in tailscale -y
sudo systemctl enable --now tailscaled
#remember to trigger authentication AFTER everything is done in this script with
#sudo tailscale up



# /$$$$$$$$ /$$             /$$                         /$$              /$$$$$$                               
#| $$_____/| $$            | $$                        | $$             /$$__  $$                              
#| $$      | $$  /$$$$$$  /$$$$$$    /$$$$$$   /$$$$$$ | $$   /$$      | $$  \ $$  /$$$$$$   /$$$$$$   /$$$$$$$
#| $$$$$   | $$ |____  $$|_  $$_/   /$$__  $$ |____  $$| $$  /$$/      | $$$$$$$$ /$$__  $$ /$$__  $$ /$$_____/
#| $$__/   | $$  /$$$$$$$  | $$    | $$  \ $$  /$$$$$$$| $$$$$$/       | $$__  $$| $$  \ $$| $$  \ $$|  $$$$$$ 
#| $$      | $$ /$$__  $$  | $$ /$$| $$  | $$ /$$__  $$| $$_  $$       | $$  | $$| $$  | $$| $$  | $$ \____  $$
#| $$      | $$|  $$$$$$$  |  $$$$/| $$$$$$$/|  $$$$$$$| $$ \  $$      | $$  | $$| $$$$$$$/| $$$$$$$/ /$$$$$$$/
#|__/      |__/ \_______/   \___/  | $$____/  \_______/|__/  \__/      |__/  |__/| $$____/ | $$____/ |_______/ 
#                                  | $$                                          | $$      | $$                
#                                  | $$                                          | $$      | $$                
#                                  |__/                                          |__/      |__/        
# 

#apps-flatpaks
#delete the # in front if you DO want to include. Didn't go fancy w/ multi file scripts, etc. Wanted noobs, like me, to 
#read this in 1 file and see what it all does in 1 file. 

#Android Studio
#flatpak install flathub com.google.AndroidStudio -y

#AnyDesk
flatpak install flathub com.anydesk.Anydesk -y

#Audacity
flatpak install flathub org.audacityteam.Audacity -y

#Brave
#flatpak install flathub com.brave.Browser -y

#Blackbox Terminal
#flatpak install flathub com.raggesilver.BlackBox -y

#Chrome
flatpak install flathub com.google.Chrome -y

#Cheese Webcam
flatpak install flathub org.gnome.Cheese -y

#Chromium
#flatpak install flathub org.chromium.Chromium -y

#Discord
#flatpak install flathub com.discordapp.Discord -y

#Disk Uage Analyzer 
flatpak install flathub org.gnome.baobab -y

#Ksnip
flatpak install flathub org.ksnip.ksnip -y

#Epiphany Web
#flatpak install flathub org.gnome.Epiphany -y

#Kdenlive
flatpak install flathub org.kde.kdenlive -y

#OnlyOffice
flatpak install flathub org.onlyoffice.desktopeditors -y

#MasterPDF Editor
flatpak install flathub net.codeindustry.MasterPDFEditor -y

#Mumble
flatpak install flathub info.mumble.Mumble -y

#OBS-Studio
flatpak install flathub com.obsproject.Studio -y

#OpenShot
#flatpak install flathub org.openshot.OpenShot -y

#Polari#
flatpak install flathub org.gnome.Polari -y

#Thunderbird Email
flatpak install flathub org.mozilla.Thunderbird -y

#Tor
flatpak install flathub com.github.micahflee.torbrowser-launcher -y

#Telegram
flatpak install flathub org.telegram.desktop -y

#Transmission
flatpak install flathub com.transmissionbt.Transmission -y

#VLC
flatpak install flathub org.videolan.VLC -y

#Wavebox
flatpak install flathub io.wavebox.Wavebox -y

#Xournal
flatpak install flathub com.github.xournalpp.xournalpp -y

#Alphabet Video Downloader ;P
flatpak install flathub me.aandrew.ytdownloader -y

#Zim Wiki
#flatpak install flathub org.zim_wiki.Zim -y

#flatpak install flathub io.gitlab.librewolf-community -y
#flatpak install flathub us.zoom.Zoom -y